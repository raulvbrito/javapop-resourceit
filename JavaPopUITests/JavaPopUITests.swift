//
//  JavaPopUITests.swift
//  JavaPopUITests
//
//  Created by Raul Brito on 08/03/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import XCTest

class JavaPopUITests: XCTestCase {
	
	// MARK: - XCTestCase
        
    override func setUp() {
        super.setUp()
		
        continueAfterFailure = false
        XCUIApplication().launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }
	
	
    // MARK: - UITests
	
    func testAppNavigation() {
		let app = XCUIApplication()

		let repositoriesTable = app.tables["Repositories"]
		var repositoryCell = repositoriesTable.cells.containing(.cell, identifier: "0").element(boundBy: 0)
		
		var start = repositoryCell.coordinate(withNormalizedOffset: CGVector(dx:0, dy:0))
		var finish = repositoryCell.coordinate(withNormalizedOffset: CGVector(dx:0, dy:2))
		start.press(forDuration: 0, thenDragTo: finish)
		
		repositoryCell = repositoriesTable.cells.containing(.cell, identifier: "3").element(boundBy: 0)
		repositoryCell.tap()

		let pullrequestsTable = app.tables["PullRequests"]
		let pullrequestCell = pullrequestsTable.cells.containing(.cell, identifier: "1").element(boundBy: 0)
		
		start = pullrequestCell.coordinate(withNormalizedOffset: CGVector(dx:0, dy:0))
		finish = pullrequestCell.coordinate(withNormalizedOffset: CGVector(dx:0, dy:2))
		start.press(forDuration: 0, thenDragTo: finish)
		
		pullrequestCell.tap()
		
		app.activate()

		app.navigationBars["Pull Requests"].buttons["Repositórios"].tap()
	}
}
