//
//  PullRequestTableViewCell.swift
//  JavaPop
//
//  Created by Raul Brito on 09/03/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

	// MARK: - Properties
	
	@IBOutlet weak var pullTitleLabel: UILabel!
	@IBOutlet weak var pullDescriptionLabel: UILabel!
	@IBOutlet weak var pullDateLabel: UILabel!
	@IBOutlet weak var authorImageView: UIImageView!
	@IBOutlet weak var authorNameLabel: UILabel!
	@IBOutlet weak var stateView: UIView!
	@IBOutlet weak var stateViewWidthConstraint: NSLayoutConstraint!
	@IBOutlet weak var stateImageView: UIImageView!
	@IBOutlet weak var stateLabel: UILabel!
	

	// MARK: - Methods
	
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setPullRequest(_ pullRequest: PullRequest) {
        if let avatar = pullRequest.owner?.avatar {
            authorImageView.sd_setImage(with: URL(string: avatar))
        } else {
            authorImageView.image = nil
        }
		
		print(pullRequest.state)
		
        if pullRequest.state == "closed" {
        	stateViewWidthConstraint.constant = 70
			stateView.backgroundColor =  UIColor(red: 203/255, green: 36/255, blue: 49/255, alpha: 1)
		} else if pullRequest.state == "merged" {
			stateViewWidthConstraint.constant = 75
			stateView.backgroundColor =  UIColor(red: 111/255, green: 66/255, blue: 193/255, alpha: 1)
		}
		
        stateImageView.image = stateImageView.image?.withRenderingMode(.alwaysTemplate)
		stateImageView.tintColor = UIColor.white
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
		dateFormatter.locale = Locale.init(identifier: "pt_BR")

		let dateObj = dateFormatter.date(from: pullRequest.date)

		dateFormatter.dateFormat = "EEEE, dd/MM 'às' HH'h'mm"
		
        pullTitleLabel.text = pullRequest.title
        pullDescriptionLabel.text = pullRequest.body
        authorNameLabel.text = pullRequest.owner?.login
        pullDateLabel.text = dateFormatter.string(from: dateObj!)
        stateLabel.text = pullRequest.state.capitalized
    }

}
