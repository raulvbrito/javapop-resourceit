//
//  RepositoryTableViewCell.swift
//  JavaPop
//
//  Created by Raul Brito on 08/03/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoryTableViewCell: UITableViewCell {

	// MARK: - Properties

	@IBOutlet weak var repNameLabel: UILabel!
	@IBOutlet weak var repDescriptionLabel: UILabel!
	@IBOutlet weak var forksCountLabel: UILabel!
	@IBOutlet weak var ratingLabel: UILabel!
	@IBOutlet weak var ownerPictureImageView: UIImageView!
	@IBOutlet weak var ownerUsernameLabel: UILabel!
	

	// MARK: - Methods

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setRepository(_ repository: Repository) {
        if let avatar = repository.owner?.avatar {
            ownerPictureImageView.sd_setImage(with: URL(string: avatar))
        } else {
            ownerPictureImageView.image = nil
        }
		
        repNameLabel.text = repository.name
        repDescriptionLabel.text = repository.description
        forksCountLabel.text = String(repository.forks)
        ratingLabel.text = String(repository.stars)
        ownerUsernameLabel.text = repository.owner?.login
    }
}
