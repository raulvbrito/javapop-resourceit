//
//  RepositoriesRequest.swift
//  JavaPop
//
//  Created by Raul Brito on 08/03/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import Foundation
import Alamofire

class RepositoriesRequest: BaseRequest {
	
    // MARK: - URLs
	
    enum URLs {
        static func getRepositories(page: Int) -> String {
            return "\(base)search/repositories?q=language:Java&sort=stars&page=\(page)"
        }
		
        static func getPulls(owner: String, repository: String) -> String {
            return "\(base)repos/\(owner)/\(repository)/pulls"
        }
    }
	
	
    // MARK: - Requests
	
    static func getRepositories(page: Int, completion: @escaping defaultArrayCallback) {
        let url = URLs.getRepositories(page: page)
        print("\nRequesting... \(url)")
		
		var errorText = "Não foi possível carregar os Repositórios. Tente novamente mais tarde"
        func requestError(code: Int?) {
			switch code {
			case 403?:
				errorText = "Não foi possível carregar mais Repositórios no momento"
				break
			default: break
			}
			
			completion(NSError(domain: errorText, code: code ?? 999, userInfo: nil), nil)
            return
        }
		
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.instance.headersSet).validate().responseJSON { (responseData) in
			
            switch responseData.result {
            case .failure(_):
                debugFailedResponse(responseData)
                requestError(code: responseData.response?.statusCode)
                break
				
            case .success(let JSON):
                //Error
                guard let result = JSON as? JSONDictionary else {
					requestError(code: responseData.response?.statusCode)
                    return
                }
				
                guard let data = result.array("items") else {
					requestError(code: responseData.response?.statusCode)
                    return
                }
				
                //Success
                completion(nil, data.flatMap(Repository.init))
                break
            }
        }
    }
	
    static func getPulls(owner: String, repository: String, completion: @escaping defaultArrayCallback) {
        let url = URLs.getPulls(owner: owner, repository: repository)
        print("\nRequesting... \(url)")
		
        var errorText = "Não foi possível carregar as Pull Requests. Tente novamente mais tarde"
        func requestError(code: Int?) {			
			completion(NSError(domain: errorText, code: code ?? 999, userInfo: nil), nil)
            return
        }
		
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.instance.headersSet).validate().responseJSON { (responseData) in
			
            switch responseData.result {
            case .failure(_):
                debugFailedResponse(responseData)
                requestError(code: responseData.response?.statusCode)
                break
				
            case .success(let JSON):
                //Error
                guard let result = JSON as? JSONArray else {
                    requestError(code: responseData.response?.statusCode)
                    return
                }

                //Success
                completion(nil, result.flatMap(PullRequest.init))
                break
            }
        }
    }
}
