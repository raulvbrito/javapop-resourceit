//
//  BaseRequest.swift
//  JavaPop
//
//  Created by Raul Brito on 08/03/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import Foundation
import Alamofire

typealias JSONDictionary = [String : Any]
typealias JSONArray = [[String : Any]]

class BaseRequest {
	
    typealias defaultArrayCallback = (_ error: NSError?, _ result: [Any]?) -> Void
    typealias defaultItemCallback = (_ error: NSError?, _ result: Any?) -> Void
	
	
    // MARK: - Config
	
    class var instance: BaseRequest {
        struct Static {
            static let instance = BaseRequest()
        }
        return Static.instance
    }
	
    static let base = "https://api.github.com/"
	
    var headersSet: HTTPHeaders = [
        "Content-Type": "application/json",
    ]
	
    static func debugFailedResponse(_ response: DataResponse<Any>) {
        print("\n== ERROR REQUEST ==")
        print("RESPONSE \(String(describing: response))")
        print("URL \(String(describing: response.request?.url))")
        print("CODE \(String(describing: response.response?.statusCode))")
        print("== == ==\n")
    }
	
}
