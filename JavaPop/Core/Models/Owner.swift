//
//  Owner.swift
//  JavaPop
//
//  Created by Raul Brito on 08/03/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import Foundation
import Tailor

struct Owner: Mappable {
    var id = Int()
    var login = String()
    var avatar = String()
	
    init(_ map: [String : Any]) {
        id <- map.property("id")
        login <- map.property("login")
        avatar <- map.property("avatar_url")
    }
}
