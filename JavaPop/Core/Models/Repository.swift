//
//  Repository.swift
//  JavaPop
//
//  Created by Raul Brito on 08/03/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import Foundation
import Tailor

struct Repository: Mappable {
    var id = Int()
    var name = String()
    var description = String()
    var stars = Int()
    var forks = Int()
    var owner: Owner?
	
    init(_ map: [String : Any]) {
        id <- map.property("id")
        name <- map.property("name")
        description <- map.property("description")
        stars <- map.property("stargazers_count")
        forks <- map.property("forks")
        owner <- map.relation("owner")
    }
}
