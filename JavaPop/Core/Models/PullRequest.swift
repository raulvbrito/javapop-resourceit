//
//  PullRequest.swift
//  JavaPop
//
//  Created by Raul Brito on 09/03/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import Foundation
import Tailor

struct PullRequest: Mappable {
    var id = Int()
    var title = String()
    var body = String()
    var date = String()
    var url = String()
    var state = String()
    var owner: Owner?
	
    init(_ map: [String : Any]) {
        id <- map.property("id")
        title <- map.property("title")
        body <- map.property("body")
        date <- map.property("created_at")
        url <- map.property("html_url")
        state <- map.property("state")
        owner <- map.relation("user")
    }
}
