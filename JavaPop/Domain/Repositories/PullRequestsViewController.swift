//
//  PullRequestsViewController.swift
//  JavaPop
//
//  Created by Raul Brito on 08/03/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import UIKit
import SVProgressHUD
import MJSnackBar

class PullRequestsViewController: UIViewController {

	// MARK: - Properties
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var noConnectionButton: UIButton!
	
	var owner: String!
	var repository: String!
	
	let refreshControl = UIRefreshControl()
	var pullRequests: [PullRequest] = []
	
	var refreshing = false
	var snackBar: MJSnackBar! = nil
	
	
	// MARK: - Methods

    override func viewDidLoad() {
        super.viewDidLoad()
		
        tableView.estimatedRowHeight = 170
		tableView.rowHeight = UITableViewAutomaticDimension
		
		instantiateRefreshControl()
		
		noConnectionButton.titleLabel?.textAlignment = NSTextAlignment.center
		
		snackBar = MJSnackBar(onView: self.view)
		snackBar.backgroundColor = UIColor.red
		snackBar.snackBarDefaultHeight = 85
		snackBar.elementsTopBottomMargins = 0
		snackBar.messageFont = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
		
		refreshData()
    }
	
    func instantiateRefreshControl() {
		refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
		tableView.refreshControl = refreshControl
		tableView.sendSubview(toBack: refreshControl)
	}
	
	@objc func refreshData() {
		if Reachability.isConnectedToNetwork(){
			tableView.isHidden = false
			noConnectionButton.isHidden = true
		
			getPulls(owner: owner, repository: repository)
		}else{
			tableView.isHidden = true
			noConnectionButton.isHidden = false
			self.refreshControl.endRefreshing()
			
			self.noConnectionButton.setTitle("Sem conexão com a internet", for: UIControlState.normal)
		}
	}
	
	func getPulls(owner: String, repository: String) {
		if refreshControl.isRefreshing == false, refreshing == false { SVProgressHUD.show() }
		
		RepositoriesRequest.getPulls(owner: owner, repository: repository) { (error, pullRequests) in
			self.refreshControl.endRefreshing()
			
			if let error = error {
				self.tableView.isHidden = true
				self.noConnectionButton.isHidden = false
				self.noConnectionButton.setTitle("Nenhum item encontrado", for: UIControlState.normal)
				
				self.snackBar.show(data: MJSnackBarData(message: error.domain), onView: self.view)
			}
			
			if let pullRequests = pullRequests as? [PullRequest] {
				self.pullRequests = pullRequests
				
				if pullRequests.isEmpty {
					self.tableView.isHidden = true
					self.noConnectionButton.isHidden = false
					self.noConnectionButton.setTitle("Nenhum item encontrado", for: UIControlState.normal)
				} else {
					self.tableView.reloadData()
				}
				
				self.refreshing = false
			}
			
			SVProgressHUD.dismiss()
		}
	}
	
	
	// MARK: - Actions
	
	@IBAction func networkRefresh(_ sender: Any) {
		//refreshData()
	}
}


// MARK: - Extensions

extension PullRequestsViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 170
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return pullRequests.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestTableViewCell") as! PullRequestTableViewCell
		cell.setPullRequest(pullRequests[indexPath.row])
		cell.accessibilityIdentifier = String(indexPath.row)
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		UIApplication.shared.open(URL(string: pullRequests[indexPath.row].url)!, options: [:], completionHandler: nil)
	}
}
