//
//  RepositoriesViewController.swift
//  JavaPop
//
//  Created by Raul Brito on 08/03/2018.
//  Copyright © 2018 Raul Brito. All rights reserved.
//

import UIKit
import SVProgressHUD
import MJSnackBar

class RepositoriesViewController: UIViewController {

	// MARK: - Properties
	
	@IBOutlet var tableView: UITableView!
	@IBOutlet weak var tableFooterView: UIView!
	@IBOutlet weak var noConnectionButton: UIButton!
	
	let refreshControl = UIRefreshControl()
	var repositories: [Repository] = []
	
	let itemsPerPage = 30
	var page = 1
	var refreshing = false
	var listEnd = false
	var snackBar: MJSnackBar! = nil
	
	
	// MARK: - Methods

	override func viewDidLoad() {
        super.viewDidLoad()
		
        self.navigationController?.navigationBar.prefersLargeTitles = true
		self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
		
		tableView.estimatedRowHeight = 200
		tableView.rowHeight = UITableViewAutomaticDimension
		
        instantiateRefreshControl()
		
        noConnectionButton.titleLabel?.textAlignment = NSTextAlignment.center
		
        snackBar = MJSnackBar(onView: self.view)
		snackBar.backgroundColor = UIColor.red
		snackBar.snackBarDefaultHeight = 85
		snackBar.elementsTopBottomMargins = 0
		snackBar.messageFont = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
		
		refreshData()
    }

    func instantiateRefreshControl() {
		refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
		tableView.refreshControl = refreshControl
		tableView.sendSubview(toBack: refreshControl)
	}
	
	@objc func refreshData() {
		page = 1
		
		if Reachability.isConnectedToNetwork(){
			tableView.isHidden = false
			noConnectionButton.isHidden = true
			
			getRepositories(page: page)
		}else{
			tableView.isHidden = true
			noConnectionButton.isHidden = false
			self.refreshControl.endRefreshing()
		}
	}
	
	func getRepositories(page: Int) {
		if refreshControl.isRefreshing == false, refreshing == false { SVProgressHUD.show() }
		
		RepositoriesRequest.getRepositories(page: page) { (error, repositories) in
			self.refreshControl.endRefreshing()
			
			if let error = error {				
				self.snackBar.show(data: MJSnackBarData(message: error.domain), onView: self.view)
			
				self.reloadTableViewFooter()
			}
			
			if let repositories = repositories as? [Repository] {				
				self.repositories = page == 1 ? repositories : self.repositories + repositories
				if repositories.isEmpty {
					self.listEnd = false
				} else if self.listEnd == false {
					self.listEnd = true
				}
				
				self.page += 1
				self.reloadTableViewFooter()
				
				self.tableView.reloadData()
				
				self.refreshing = false
			}
			
			SVProgressHUD.dismiss()
		}
	}
	
	func reloadTableViewFooter() {
		tableFooterView.frame = listEnd && repositories.count > itemsPerPage ? CGRect(x: 0, y: 0, width: view.frame.width, height: 200) : .zero
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "PullRequestsSegue" {
			let vc = segue.destination as? PullRequestsViewController
			vc?.owner = (sender as! Repository).owner?.login
			vc?.repository = (sender as! Repository).name
		}
	}
	
	
	// MARK: - Actions
	
	@IBAction func networkRefresh(_ sender: Any) {
		refreshData()
	}
}


// MARK: - Extensions

extension RepositoriesViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 200
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return repositories.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryTableViewCell") as! RepositoryTableViewCell
		cell.setRepository(repositories[indexPath.row])
		cell.accessibilityIdentifier = String(indexPath.row)
		
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		performSegue(withIdentifier: "PullRequestsSegue", sender: repositories[indexPath.row])
	}
}

extension RepositoriesViewController: UIScrollViewDelegate {
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
		let count = repositories.count
		
		if (bottomEdge >= scrollView.contentSize.height - 2400) {
			if listEnd, refreshing == false, count >= itemsPerPage  {
				refreshing = true
				
				if Reachability.isConnectedToNetwork(){
					getRepositories(page: page)
				}else{
					self.snackBar.show(data: MJSnackBarData(message: "Sem conexão com a internet"), onView: self.view)
				}
			}
		}
	}
}
